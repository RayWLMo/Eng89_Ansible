![Ansible Diagram](https://gitlab.com/RayWLMo/Eng89_Ansible/-/raw/main/Ansible.png)


# Ansible controller and agent nodes set up guide
- Clone this repo and run `vagrant up`
- `(double check syntax/intendation)`

## We will use 18.04 ubuntu for ansible controller and agent nodes set up
### Please ensure to refer back to your vagrant documentation

- **You may need to reinstall plugins or dependencies required depending on the OS you are using.**

```vagrant
# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what

# MULTI SERVER/VMs environment
#
Vagrant.configure("2") do |config|

# creating first VM called web  
  config.vm.define "web" do |web|

    web.vm.box = "bento/ubuntu-18.04"
   # downloading ubuntu 18.04 image

    web.vm.hostname = 'web'
    # assigning host name to the VM

    web.vm.network :private_network, ip: "192.168.33.10"
    #   assigning private IP

    config.hostsupdater.aliases = ["development.web"]
    # creating a link called development.web so we can access web page with this link instread of an IP   

  end

# creating second VM called db
  config.vm.define "db" do |db|

    db.vm.box = "bento/ubuntu-18.04"

    db.vm.hostname = 'db'

    db.vm.network :private_network, ip: "192.168.33.11"

    config.hostsupdater.aliases = ["development.db"]     
  end

 # creating are Ansible controller
  config.vm.define "controller" do |controller|

    controller.vm.box = "bento/ubuntu-18.04"

    controller.vm.hostname = 'controller'

    controller.vm.network :private_network, ip: "192.168.33.12"

    config.hostsupdater.aliases = ["development.controller"]

  end

end
```

## Ansible commands

- access to ansible config folder `cd /etc/ansible`
- `sudo nano hosts` to add IP instances

```
[web]
192.168.33.10 ansible_connection=ssh ansible_ssh_user=vagrant ansible_ssh_pass=vagrant` - add to hosts file
```

- `ansible ssh IP_ADDRESS` to manually connect to machine
- `ansible web -m ping` to ping machine
  - successful response `pong`
- `ansible all` - runs command to all machines in hosts folder
  - `ansible all -a 'uname -a'
- `ansible all -a "free -m"` - checks free memory
- `ansible web -m command -a uptime` - check uptime of the machine
-

Nginx Playbook
```yml
# This is a playbook to install and set up Nginx in the web server (192.168.33.10)
# This playbook is written in YAML. YAML starts with `---`
---
# name of the host - hosts is to define the name of your host or all
- hosts: web
# find the facts about the host

  gather_facts: yes

# admin access
  become: true

# instructions using tasks module in ansible
  tasks:
  - name: install nginx  -
# install nginx
    apt: name=nginx state=present update_cache=yes
# ensure it's running/active
# update cache
# restart nginx if reverse proxy is implemented or if needed
    notify:
      - restart nginx
  - name: Allow all access to tcp port 80
    ufw:
        rule: allow
        port: '80'
        proto: tcp

  handlers:
    - name: Restart Nginx
      service:
        name: nginx
        state: restarted
```

- `ansible-playbook nginx_playbook.yml` - to run the playbook
- `ansible web -m shell -a 'systemctl status nginx'` -to check nginx status

Installing nodejs using Ansible
```yml
---
# name of the host - hosts is to define the name of your host or all
- hosts: web
# find the facts about the host

  gather_facts: yes

# admin access
  become: true

# instructions using tasks module in ansible
  tasks:
  - name: "Add nodejs apt key"
    apt_key:
      url: https://deb.nodesource.com/gpgkey/nodesource.gpg.key
      state: present

  - name: "Add nodejs 13.x ppa for apt repo"
    apt_repository:
      repo: deb https://deb.nodesource.com/node_13.x bionic main
      update_cache: yes

  - name: "Install node.js"
    apt: name=nodejs
         state=present
         update_cache=yes

  - name: Cloning app folder to web machine
    ansible.builtin.git:
      repo: https://gitlab.com/RayWLMo/Virtual_Machines.git
      dest: /home/vagrant/app/
      version: app

  - name: NPM installation
    shell: npm i
    args:
      chdir: /home/vagrant/app/app/

  - name: Deleting old default file
    file:
      state: absent
      path: /etc/nginx/sites-available/default

  - name: Creating new defaut file
    file:
      path: /etc/nginx/sites-available/default
      state: touch
      mode: u=rw,g=r,o=r

  - name: Changing default file
    blockinfile:
      path: /etc/nginx/sites-available/default
      block: |
        server{
          listen 80;
          server_name _;
          location / {
            proxy_pass http://192.168.33.10:3000;
            proxy_http_version 1.1;
            proxy_set_header Upgrade \$http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host \$host;
            proxy_cache_bypass \$http_upgrade;
            }
          }

  - name: Restarting nginx
    service:
      name: nginx
      state: restarted

  - name: Running NPM
    shell: npm start
    args:
      chdir: /home/vagrant/app/app/
```

Installing mongodb on the db vagrant machine

```yml
# Setting up MongoDB on the db server

---
# Specifyinh host
- hosts: db

# Gathering facts
  gather_facts: yes

# Giving admin priviledges
  become: true

# Tasks to install and setup mongoDB
  tasks:

# Installing MongoDB package
  - name: install mongodb
    apt: pkg=mongodb state=present
# Removing the old mongodb file
  - name: Remove mongodb file (delete file)
    file:
      path: /etc/mongodb.conf
      state: absent
# Creating the new mongod.conf file
  - name: Touch a file, using symbolic modes to set the permissions (equivalent to 0644)
    file:
      path: /etc/mongodb.conf
      state: touch
      mode: u=rw,g=r,o=r

# Adding relevant code to mongodb.conf file
  - name: Insert multiple lines and Backup
    blockinfile:
      path: /etc/mongodb.conf
      backup: yes
      block: |
        storage:
          dbPath: /var/lib/mongodb
          journal:
            enabled: true
        systemLog:
          destination: file
          logAppend: true
          path: /var/log/mongodb/mongod.log
        net:
          port: 27017
          bindIp: 0.0.0.0

# enable mongodb with admin access
  - name: enable mongodb
    become: true
    shell: systemctl enable mongodb

# restart mongodb with admin access
  - name: restart mongodb
    become: true
    shell: systemctl restart mongodb
```

# Ansible with the cloud

- create a new folder in /etc/ansible called group_vars
- inside group_vars, create a folder called all
- once in the all folder, enter the command `sudo ansible-vault create pass.yml`
- This will prompt for a password, which can be anything
- After the password is set,it will open the `pass.yml` in a vim editor
- Add in the AWS access and secret key in this format:
```
aws_access_key: ADD YOUR AWS ACCESS KEY HERE
aws_secret_key: ADD YOUR AWS SECRET KEY HERE
```
- creating the `create_ec2.yml` file

```yml

# playbook for launchin an aws ec2 instance

---
- hosts: localhost
  connection: local
  gather_facts: True
  become: True
  vars:
    key_name: eng89_devops
    region: eu-west-1
    image: ami-039900c4ef89c6f9c
    id: "eng89 ansible playbook to launch an aws ec2 instance"
    sec_group: "sg-0e4a4d95cfa2c3ec0"
    subnet_id: "subnet-00ac052b1e40c0164"
    ansible_python_interpreter: /usr/bin/python3
  tasks:

    - name: facts
      block:

      - name: get instance gather_facts
        ec2_instance_facts:
          aws_access_key: "{{aws_access_key}}"
          aws_secret_key: "{{aws_secret_key}}"
          region: "{{ region }}"
        register: result

    - name: provisioning ec2 instances
      block:

      - name: upload public key to aws_access_key
        ec2_key:
          name: "{{ key_name }}"
          key_material: "{{ lookup('file', ~./ssh/{{ key_name }}.pub') }}"
          region: "{{ region }}"
          aws_access_key: "{{aws_access_key}}"
          aws_secret_key: "{{aws_secret_key}}"

      - name: provision instance
        ec2:
          aws_access_key: "{{aws_access_key}}"
          aws_secret_key: "{{aws_secret_key}}"
          assign_public_ip: True
          key_name: "{{ key_name }}"
          id: "{{ id }}"
          vpc_subnet_id: "{{ subnet_id }}"
          group_id: "{{ sec_group }}"
          image: "{{ image }}"
          instance_type: t2.micro
          region: "{{ region }}"
          wait: True
          count: 1
          instance_tags:
            Name: eng89_ray_ansible_playbook

      tags: ['never', 'create_ec2']
```
